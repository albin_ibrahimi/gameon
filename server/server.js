const express = require('express');
const  bodyParser = require('body-parser');
const  cors = require('cors');
const  mongoose = require('mongoose');
const  config = require('./DB');
const  news = require ('./routes/news');
const  contact = require ('./routes/contact');
const  games = require ('./routes/games');
const adv  = require('./routes/adv');
const  category = require ('./routes/category');
const  trailers = require ('./routes/trailers');

mongoose.Promise = global.Promise;
mongoose.connect(config.DB);
      
const app = express();
app.use(bodyParser.json());
app.use(cors());
app.use('/news', news);
app.use('/games', games);
app.use('/contact', contact);
app.use('/adv', adv);
app.use('/category', category);
app.use('/trailers', trailers);

var port = process.env.PORT || 4000;

app.listen(port, function(){
  console.log('NodeJS Server Port: ', port);
});