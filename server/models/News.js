const mongoose = require('mongoose');

const NewsSchema = mongoose.Schema({
    title: String,
    content:String,
    bigimage:String,
    mediumimage:String,
    smallimage:String,
    category: String,
    updated_date: { type: Date, default: Date.now },
});
module.exports = mongoose.model('News', NewsSchema);