const mongoose = require('mongoose');

const TrailersSchema = mongoose.Schema({
    title: String,
    image: String,
    video: String,
    videofile: String,
    updated_date: { type: Date, default: Date.now },
});
module.exports = mongoose.model('Trailers', TrailersSchema);