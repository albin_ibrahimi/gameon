const mongoose = require('mongoose');

const AdvSchema = mongoose.Schema({
    title: String,
    description: String,
    bigimage:String,
    mediumimage:String,
    smallimage:String,
    updated_date: { type: Date, default: Date.now },
});
module.exports = mongoose.model('Adv', AdvSchema);