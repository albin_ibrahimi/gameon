const mongoose = require('mongoose');

const GamesSchema = mongoose.Schema({
    name:String,
	description:String,
    image:String,
    category: String,
    year: Number,
    company: String,
    age: Number,
    updated_date: { type: Date, default: Date.now },
});
module.exports = mongoose.model('Games', GamesSchema);