import Vue from 'vue'
import App from './App.vue'
import VueRouter from 'vue-router'
import * as firebase from "firebase";
import store from "./store";
import VuePlyr from 'vue-plyr';
import router from './router';
// The second argument is optional and sets the default config values for every player.
Vue.use(VuePlyr, {
  plyr: {
    fullscreen: { enabled: false }
  },
  emit: ['ended']
})

Vue.config.productionTip = false

const configOptions = {
  apiKey: "AIzaSyA91v6bOBjggN4lnBJOqF_IHXHERLpcl_k",
    authDomain: "gameon-db.firebaseapp.com",
    databaseURL: "https://gameon-db.firebaseio.com",
    projectId: "gameon-db",
    storageBucket: "gameon-db.appspot.com",
    messagingSenderId: "781758476337",
    appId: "1:781758476337:web:51a18c79f9aca355f6e510"
};

import VueAnime from 'vue-animejs';
 
Vue.use(VueAnime)


firebase.initializeApp(configOptions);

Vue.use(VueRouter);



let app;
firebase.auth().onAuthStateChanged(function(user){
  store.dispatch("fetchUser", user);
  if(!app){
  app = new Vue({
  el: '#app',
  store,
  router,
  render: h => h(App)
  });
}
});
