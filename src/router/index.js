import Dashboard from '@/components/Dashboard/Dashboard';
import Adv from '@/components/Advertise/Adv';
import AddCategory from '@/components/Category/AddCategory';
import CategoryEdit from '@/components/Category/CategoryEdit';
import Home from '@/components/Home/Home';
import News from '@/components/News/News';
import NewsList from '@/components/NewsList/NewsList';
import GamesList from '@/components/GamesList/GamesList';
import ReadNews from '@/components/ReadNews/ReadNews';
import Login from '@/components/Admin/Login';
import Register from '@/components/Admin/Register';
import AddTrailer from '@/components/AddTrailer/AddTrailer';
import WatchTrailer from '@/components/WatchTrailer/WatchTrailer';
import Trailers from '@/components/Trailers/Trailers';
import AboutUs from '@/components/AboutUs/AboutUs';
import Contact from '@/components/Contact/Contact';
import GameDetails from '@/components/Games/GameDetails';
import AdminPanel from '@/components/AdminPanel/AdminPanel';
import CategoryPage from '@/components/CategoryPage/CategoryPage';
import AddGame from '@/components/Games/AddGame';
import Games from '@/components/NewGames/Games';
import Messages from '@/components/Messages/Messages';
import ReadMessage from '@/components/Messages/ReadMessage';
import Vue from 'vue';
import Router from 'vue-router';
import firebase from 'firebase';
//importojme gjithe ato komponenta qe do i regjistrojme si routes
Vue.use(Router);

let router = new Router({
 routes : [
    
  { path:'/dashboard', name:'dashboard', component:Dashboard, meta:{requiresAuth:true}},
  { path:'/adv', name:'adv', component:Adv,meta:{requiresAuth:true}},
  { path:'/addcategory', name:'addcategory', component:AddCategory,meta:{requiresAuth:true}},
  { path:'/categoryedit/:Cid', name:'categoryedit', component:CategoryEdit,meta:{requiresAuth:true}},
  { path:'/', name:'home', component:Home},
  { path:'/login', name:'login', component:Login,meta:{requiresGuest:true}},
  { path:'/register', name:'register', component:Register,meta:{requiresGuest:true}},
  { path:'/news', name:'news', component:News},
  { path:'/newslist', name:'newslist', component:NewsList, meta:{requiresAuth:true}},
  { path:'/gameslist', name:'gameslist', component:GamesList, meta:{requiresAuth:true}},
  { path:'/readnews/:Nid', name:'readnews', component:ReadNews},
  { path:'/addtrailer', name:'addtrailer', component:AddTrailer,meta:{requiresAuth:true}},
  { path:'/addgame', name:'addgame', component:AddGame,meta:{requiresAuth:true}},
  { path:'/messages', name:'messages', component:Messages,meta:{requiresAuth:true}},
  { path:'/readmessage/:Mid', name:'readmessage', component:ReadMessage,meta:{requiresAuth:true}},
  { path:'/trailers', name:'trailers', component:Trailers},
  { path:'/aboutus', name:'aboutus', component:AboutUs},
  { path:'/contact', name:'contact', component:Contact},
  { path:'/gamedetails/:Gid', name:'gamedetails', component:GameDetails},
  { path:'/adminpanel', name:'adminpanel', component:AdminPanel,meta:{requiresAuth:true}},
  { path:'/watchtrailer/:Tid', name:'watchtrailer', component:WatchTrailer},
  { path:'/categorypage/:Cid', name:'categorypage', component:CategoryPage},
  { path:'/games', name:'games', component:Games}
  ],
  scrollBehavior() {
    window.scrollTo(0,0);
  }

});

router.beforeEach((to, from, next) => {
    // Check for requiresAuth guard
    if (to.matched.some(record => record.meta.requiresAuth)) {
      // Check if NO logged user
      if (!firebase.auth().currentUser) {
        // Go to login
        next({
          path: '/login',
          
        });
      } else {
        // Proceed to route
        next();
      }
    } else if (to.matched.some(record => record.meta.requiresGuest)) {
      // Check if NO logged user
      if (firebase.auth().currentUser) {
        // Go to login
        next({
          path: '/',
        
        });
      } else {
        // Proceed to route
        next();
      }
    } else {
      // Proceed to route
      next();
    }
  });
  
  export default router;